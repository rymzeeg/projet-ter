package sujet;

public class Etudiant extends Personne{


	private Groupe g;
	
	public Etudiant(int num, String nom, String prenom,Groupe g) {
		super(num, nom, prenom);
		this.g=g;
	}


	public Groupe getG() {
		return this.g;
	}

	public void setG(Groupe g) {
		this.g = g;
	}
	
	//pas demandé à supprimer
	public void participe() throws IllegalArgumentException{
		if(this.getG()==null) {
			throw new IllegalArgumentException("l'étudiant doit appartenir à un groupe");}
	}
	
	
	
	
}
